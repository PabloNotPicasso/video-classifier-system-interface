# Protobuf generator

[[_TOC_]]
## Summary
This repository generates sources from protobuf interface files

## Dependencies
[gcc](https://gcc.gnu.org/) \
[cmake](https://cmake.org/) \
[Python 3](https://www.python.org/download/releases/3.0/) \
[gRPC](https://github.com/grpc/grpc) \
[Protobuf](https://github.com/protocolbuffers/protobuf)

## Build

### Build library
```
$ cmake -S . -Bbuild
$ cmake --build build
```
### Generate service from .proto
```
cmake -S . -Bbuild -DENEBLE_GENERATION=ON
cmake --build build
```
### Build test service
```
cmake -S . -Bbuild -DBUILD_TEST_SERVICE=ON
cmake --build build
```