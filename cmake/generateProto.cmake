find_package (Protobuf REQUIRED)
find_package (GRPC REQUIRED)
find_package (gRPC REQUIRED)

message("Generate Function declared")

function(generate_proto PROTO_DIR PROTO_NAME GEN_DIR)
    PROTOBUF_GENERATE_CPP(PROTO_SRCS PROTO_HDRS ${PROTO_DIR}/${PROTO_NAME}.proto)
    PROTOBUF_GENERATE_GRPC_CPP(GRPC_SRCS GRPC_HDRS ${PROTO_DIR}/${PROTO_NAME}.proto)

    set(GEN_SRC 
        ${PROTO_NAME}.pb.cc
        ${PROTO_NAME}.pb.h
        ${PROTO_NAME}.grpc.pb.cc
        ${PROTO_NAME}.grpc.pb.h
        )
    
    add_library( dummy_${PROTO_NAME} ${GEN_SRC})
    target_link_libraries( dummy_${PROTO_NAME} ${PROTOBUF_LIBRARIES} ${GRPC_LIBRARIES})
endfunction()
